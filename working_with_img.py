"""
Модуль работы с изображениями
"""
import logging

from PIL import ImageGrab


logger = logging.getLogger('img_module')


def screen(path_img: str, file_name='screen') -> None:
    """Функция делает скриншоты"""
    img = ImageGrab.grab()
    img.save(f'{path_img}/{file_name}.png', 'PNG')
    logger.debug(f'Сделан и сохранён скрин {file_name}')
