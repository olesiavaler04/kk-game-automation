import logging
import sys

from pywinauto import application, keyboard
import pyuac

from working_with_img import screen
from game_action import go_to_second_scene, start_game, quit_game
from fraps_connect import connect_to_fraps, get_last_info_file, save_result


logger = logging.getLogger('main')

def main():
    logger.info('Начало теста')
    connect_to_fraps()
    logger.debug('Fraps подключился')

    app = application.Application(backend='uia')
    logger.info('Запуск kkrieger')
    app.start(sys.argv[1])

    start_game()
    screen(path_img=sys.argv[-1], file_name='start_game')
    logger.info('Сделан скриншот начала тестирования')
    keyboard.send_keys('{F11 down} {F11 up}')  # кнопка по умолчанию в Fraps
    logger.info('Запуск снятия статистики')
    app['kk'].set_focus()
    go_to_second_scene()  # движение игрока
    keyboard.send_keys('{F11 down} {F11 up}')
    logger.info('Завершение снятия статистики')
    screen(path_img=sys.argv[-1], file_name='end_game')
    logger.info('Сделан скриншот конца тестирования')
    quit_game()

    fps_state_file = get_last_info_file()
    save_result(fps_state_file, sys.argv[-1])


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)s| %(asctime)s |%(name)s - %(message)s',
                        datefmt="%d-%m-%Y %H:%M:%S",
                        handlers=[logging.FileHandler(filename=f'{sys.argv[-1]}\kkrieger.log',
                                                      encoding='utf-8', mode='a')]
                        )

    if not pyuac.isUserAdmin():
        logger.debug('Запуск происходит не от имени администратора')
        pyuac.runAsAdmin()
    else:
        try:
            main()
        except Exception as e:
            logger.error(f'Ошибка: {e}', exc_info=e)
        finally:
            logger.info('Тест окончен')
