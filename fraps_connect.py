"""
Модуль для работы с программой Fraps
"""
import csv
import os
import logging
import shutil

from pywinauto import mouse
from pywinauto.application import Application, ProcessNotFoundError
from pywinauto.controls.win32_controls import ButtonWrapper


logger = logging.getLogger('fraps')


def connect_to_fraps() -> None:
    """Функция осуществляет подключение и работу с программой Fraps"""
    app = Application()
    try:
        logger.debug('Поиск Fraps среди запущенных программ и попытка подключения')
        app.connect(path=r'C:\Program Files\fraps.exe')  # путь по умолчанию при установке программы
    except ProcessNotFoundError:
        logger.debug('Подключиться не удалось. Запуск Fraps')
        app.start(r'C:\Program Files\fraps.exe')
    finally:
        logger.info('Fraps запущен')
        find_fps_window(app)
        logger.debug('Открыта вкладка FPS')
        use_fps_window(app)
        logger.debug('Прогорамма Fraps готова к работе')


def find_fps_window(app: Application) -> None:
    """Функция ищет и переключается на вкладку FPS"""
    logger.debug('Поиск открытого окна Fraps')
    dlg = app.window(title_re='FRAPS')
    rect = dlg.rectangle()
    x = rect.left
    y = rect.top
    logger.debug('Контроль окна и клик по вкладке FPS')
    dlg.set_focus()
    mouse.click(button='left', coords=(x + 225, y + 50))


def use_fps_window(app: Application) -> None:
    """Функция проверяет необходимый флаг для записи результатов в окне FPS"""
    dlg = app['FRAPS fps']

    checkbox = ButtonWrapper(dlg.FPS.wrapper_object())
    if not checkbox.is_checked():
        logger.debug('Устанавливаем флаг FPS')
        checkbox.click()


def get_last_info_file(path=r'C:\Program Files\Benchmarks') -> str:
    """Функция ищет необходимый файл для обработки результатов"""
    files = os.listdir(path)
    files = [os.path.join(path, file) for file in files if file.endswith('fps.csv')]
    info_file = max(files, key=os.path.getctime)
    logger.debug(f'Найденный файл: {info_file}')
    return info_file


def save_result(state_file: str, path: str) -> None:
    """Функция сохраняет результаты в указанную папку"""
    new_filename = state_file[-38:].replace('pno0001', 'kkrieger')
    shutil.copyfile(state_file, fr'{path}\{new_filename}')
    avg_fps = get_avg_fps(state_file)

    with open(fr'{path}\avg_fps.csv', 'a') as file:
        logger.debug('Запись в файл avg_fps.csv')
        writer = csv.writer(file)
        writer.writerow([avg_fps, new_filename])

    logger.info('Сохранение статистики FPS')


def get_avg_fps(state_file: str) -> float:
    """Функция высчитывает средний показатель FPS на основании
    полученных результатов, записанных программой"""
    with open(state_file, 'r') as file:
        fps = 0
        lines = 0
        reader = csv.reader(file)
        for line in reader:
            if line[0].isdigit():
                fps += int(line[0])
                lines += 1
        logger.debug(f'Вычислено среднее значение FPS: {round(fps / lines, 2)}')
        return round(fps / lines, 2)
