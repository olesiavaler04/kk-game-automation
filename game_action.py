"""
Модуль отвечает за действия в окне игры
"""
import logging
import time

from pywinauto import keyboard


logger = logging.getLogger('game')


def start_game() -> None:
    """Функция ожидает загрузку игры и пропускает заставку"""
    time.sleep(20)
    for _ in range(2):
        keyboard.send_keys('{ENTER down} {ENTER up}')
    logger.info('Игра запустилась на сцену')


def quit_game() -> None:
    """Функция осуществляет завершение и выход из игры"""
    keyboard.send_keys('{VK_ESCAPE down} {VK_ESCAPE up}')
    for _ in range(2):
        keyboard.send_keys('{DOWN down}  {DOWN up}')
        keyboard.send_keys('{DOWN down}  {DOWN up}')
        keyboard.send_keys('{ENTER down} {ENTER up}')

    logger.info('Выход из игры и закрытие окна')


def go_to_second_scene() -> None:
    """Функция осуществляет продвижение игрока"""
    logger.info('Начало движения игрока')
    keyboard.send_keys('{w down}')
    keyboard.send_keys('{a down}')
    time.sleep(1)
    keyboard.send_keys('{a up}')
    time.sleep(3)
    keyboard.send_keys('{d down}')
    time.sleep(1)
    keyboard.send_keys('{d up}')
    time.sleep(1)
    keyboard.send_keys('{a down} {a up}')
    time.sleep(5)
    keyboard.send_keys('{w up}')
    logger.info('Конец движения игрока')
